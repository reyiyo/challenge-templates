/*
 * Checks if the free available space of the cluster is lesser than treshold
 * and add 2 more nodes to the cluster
 */

require('dotenv').config();
const request = require('request-promise-native');
const AWS = require('aws-sdk');
AWS.config.update({ region: process.env.AWS_REGION });
const ESS = new AWS.ES();

const endpoint = `${process.env.ES_URL}/_cluster/stats`;
const threshold = process.env.FREE_THRESHOLD;
const nodeScaleCount = 2;
const domainName = process.env.DOMAIN_NAME;

const reqParams = { json: true };

if (typeof process.env.ES_USER !== 'undefined' && process.env.ES_USER !== null) {
    reqParams.auth = {
        user: process.env.USER,
        pass: process.env.PASSWORD
    };
}

request
    .get(endpoint, reqParams)
    .then(data => {
        const totalBytes = data.nodes.fs.total_in_bytes;
        const availableBytes = data.nodes.fs.available_in_bytes;
        const availablePercent = availableBytes * 100 / totalBytes;
        console.log(`Total: ${totalBytes} - Available: ${availableBytes} - Porcentage: ${availablePercent} `);
        if (availablePercent <= threshold) {
            ESS.describeElasticsearchDomainConfig({ DomainName: domainName }, (err, data) => {
                if (err) {
                    console.error(err);
                    return;
                }
                const state = data.DomainConfig.ElasticsearchClusterConfig.Status.State;
                if (state === 'Active') {
                    const instanceCount = data.DomainConfig.ElasticsearchClusterConfig.Options.InstanceCount;
                    const newInstanceCount = instanceCount + nodeScaleCount;
                    ESS.updateElasticsearchDomainConfig(
                        {
                            DomainName: domainName,
                            ElasticsearchClusterConfig: {
                                InstanceType: process.env.INSTANCE_TYPE,
                                InstanceCount: newInstanceCount
                            }
                        },
                        (err, data) => {
                            if (err) {
                                console.error(err);
                                return;
                            }
                        }
                    );
                }
            });
        }
    })
    .catch(error => {
        console.error(error);
    });
