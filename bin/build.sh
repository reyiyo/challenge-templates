#!/bin/sh

set -o errexit -o xtrace

REGION=us-east-1
ACCOUNT_ID=994496604930

REGISTRY_URL="${ACCOUNT_ID}.dkr.ecr.${REGION}.amazonaws.com"

aws ecr create-repository --repository-name elasticsearch-config --region $REGION || true
$(aws ecr get-login --region $REGION --no-include-email)

docker build -t elasticsearch-config elasticsearch-config/

docker tag elasticsearch-config ${REGISTRY_URL}/elasticsearch-config:latest

docker push ${REGISTRY_URL}/elasticsearch-config:latest

aws ecr create-repository --repository-name elasticsearch-scaling --region $REGION || true

docker build -t elasticsearch-scaling elasticsearch-scaling/

docker tag elasticsearch-scaling ${REGISTRY_URL}/elasticsearch-scaling:latest

docker push ${REGISTRY_URL}/elasticsearch-scaling:latest