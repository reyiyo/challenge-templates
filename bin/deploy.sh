#!/bin/sh

set -o errexit -o xtrace

bucket=meli-challenge-templates
regions=(
  us-east-1
)

bin/build.sh

for region in "${regions[@]}"
do
  aws s3api head-bucket --bucket "${bucket}" --region "$region" ||
    aws s3 mb "s3://${bucket}" --region "$region"

  aws s3api put-bucket-policy \
    --bucket "${bucket}" \
    --policy "{\"Version\":\"2012-10-17\",\"Statement\":[{\"Effect\":\"Allow\",\"Principal\":\"*\",\"Action\":[\"s3:GetObject\",\"s3:GetObjectVersion\"],\"Resource\":\"arn:aws:s3:::${bucket}/*\"},{\"Effect\":\"Allow\",\"Principal\":\"*\",\"Action\":[\"s3:ListBucket\",\"s3:GetBucketVersioning\"],\"Resource\":\"arn:aws:s3:::${bucket}\"}]}" \
    --region "$region"

  aws s3api put-bucket-versioning \
    --bucket "${bucket}" \
    --versioning-configuration Status=Enabled \
    --region "$region"

  aws s3 cp --recursive infra/ "s3://${bucket}/infra" --region "$region"
  aws s3 cp --recursive logging-services/ "s3://${bucket}/logging-services" --region "$region"
  aws s3 cp --recursive reports/ "s3://${bucket}/reports" --region "$region"
  aws s3 cp master.yaml "s3://${bucket}/master.yaml" --region "$region"
done