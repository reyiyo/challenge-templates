# Challenge Templates

## Overview

El objetivo es generar un dashboard con reportes "near real time" (2 minutos máximo) para las capas de balanceo de carga, que tienen una carga media de 40.000.000 rpm y 500 pooles de backend.

Para esto, utilizaremos las siguientes teconologías:

* Elasticsearch + Kibana: Para indexar los logs y generar los reportes.
* Kinesis + Kinesis Firehose: Como punto de entrada de los datos y para procesar lo que sea necesario antes de indexar.
* S3: Como storage para los logs históricos.


## Data Pipeline

![Infraestructura](assets/data-pipeline.png)

**Kinesis** es una solución de procesamiento de streams de datos como logs. Facilita la recopilación, el procesamiento y el análisis de datos de streaming en tiempo real. Es una parte necesaria de la solución, para poder recibir de forma confiable y en tiempo real todos los logs que se envíen. Es fácil de integrar a soluciones existentes, ya que existen agentes para enviar los logs de un servidor, además de plugins para soluciones como cloudwatch y logstash.

**Kinesis Firehose** es una solución que permite procesar los datos que llegan streaming y transmitirlos a diferentes lugares. En nuestro caso, elasticsearch y S3.

**Lambda** Es una función de Amazon Lambda que parsea las líneas de log y las devuelve de forma estructurada para indexar en elasticsearch.

Al salir de Firehose, los datos ya formateados se envían a elasticsearch para generar los reportes y a S3 para tener un histórico de los logs.

Luego de un tiempo configurable, los logs guardados en S3 pasan a ser almacenados en Glacier para optimizar costos.

El cluster de elasticsearch almacena los logs indexados por una cantidad tiempo configurable. Los mismos están disponibles para ver y buscar en un dashboard de Kibana. Una vez superado este tiempo, se pueden consultar desde S3 en caso de necesitarlos, a través de tecnologías de big data como Hadoop, Athena, Redshift, etc.


## Servicios auxiliares:

* `elasticsearch-config`: Configura en el cluster de elasticsearch los templates para los índices y las visualizaciones de Kibana.
* `index-curator`: Tarea periódica que corre cada el mismo tiempo de rotación de los índices. Lo que hace es optimizar los segmentos de los índices anteriores al actual y borrar los índices más viejos que el período de retención establecido.
* `elasticsearch-scaling`: Es una tarea periódica que corre cada un tiempo configurable. Lo que hace es monitorerar el espacio disponible en disco de todo el cluster. Cuando este valor es menor a un procentaje definido, agrega nodos de datos al cluster.

## Dimensionamiento del cluster de elasticsearch

Como pre-condición, hay que tener en cuenta que la carga va a ser mayormente de indexación y muy poca para servir queries. Con el volumen de datos propuesto, lo más probable es que la cantidad de nodos mínimos necesarios para el cluster esté determinada por la capacidad de storage necesaria para guardar esos datos por el tiempo definido. Aunque también hay que probar que esta cantidad de nodos resultante sea suficiente para procesar el indexado de 40M de documentos por minuto.

### Storage

La prueba realizada para tener una idea inicial del tamaño de los datos fue crear un cluster de 1 nodo, con 1 índice de 1 shard. Cargarlo con 1M de registros de ejemplo y optimizar el shard a 1 segmento como máximo para obtener un valor constante de comparación.

Lo primero que se comprobó es que el tamaño total crece de forma lineal a medida que crece la cantidad de docs indexados (no guardé los datos, pero lo comprobé).

El resultado obtenido al indexar 1 millón de documentos con los valores por default de elasticsearch fue de un tamaño total del índice de ~436 MB.

Esto nos daría la siguiente cuenta:

```
1M registros = 436 MB
40M registros = 17 GB

40M registros/minuto * 60 minutos * 24 horas = 24 TB/día
```

El siguiente paso fue realizarle las siguientes optimizaciones al índice:

* Desactivar el campo _all del índice, porque las búsquedas que vamos a hacer son por campos definidos.
* Desactivar el campo _source del índice, ya que nuestros documentos indexados no se actualizan y es muy probable que los índices tampoco vayan a hacerlo.
* Crear un Mapping para que cada campo del documento use el tipo de dato correcto y el mínimo necesario.

El resultado de correr la misma prueba fue un índice de ~ 110 MB. Lo que nos deja con la siguiente cuenta:

```
1M registros = 110 MB
40M registros = 4.4 GB

40M registros/minuto * 60 minutos * 24horas = 6 TB/día
```

Suponemos que queremos tener 1 réplica para una mínima tolerancia a fallas de los nodos sin perder datos, por lo que daría un total de 12 TB/día.

El tamaño máximo de volumen que admiten las instacias más grandes de elasticsearch service es de 1,5 TB / nodo. Por lo que como mínimo para cumplir con los requisitos de storage para un día, necesitaríamos 8 nodos.

Además, convendría tener los nodos master separados de los nodos de datos, por lo que habría que sumarle 3 nodos más.

Lo que evaluaría es si se quiere guardar todos los campos del log o se podría reducir la cantidad de campos que interesan. De esta forma se podrían reducir los requerimientos de storage.

### Índices

Como el patrón de datos es de tipo time-series y lo más probable es que los datos más nuevos sean más consultados que los viejos, conviene dividir los índices por una ventana de tiempo. Puede ser un índice por día o por hora. Esto permitiría borrar los registros viejos borrando un índice completo.

### Shards

Según lo que recomiendan, conviene que los shards no superen los 50 GB por el tema de mover shards de un lado a otro y la cantidad de datos a procesar para una query. Teniendo en cuenta esto y que la cantidad de datos por shard es "pareja" (que no hay un shard más grande que otro):

* Si tuviéramos un índice por día, para un tamaño de shard de 30GB resultaría en 200 shards por índice.
* Si tuviéramos un índice por hora, podríamos tener 10 shards por índice de 27GB cada una.

Como la carga de queries es probable que siempre esté en el último índice (el más nuevo) y que en los dashboards como caso general se quiera ver siempre los datos de la última hora, convendría tener un índice por hora para que la carga de hacer el "join" de las queries sea menor.

### Nodos y hardware

Para poder especificar bien esto, tendría que lograr hacer las pruebas con la carga real. Empezaría con un cluster de 8 data nodes + 3 masters y mediría los recursos consumidos para procesar el indexado (principalmente, CPU, RAM, latencia de disco y las colas de elasticsearch). Probaría primero con instancias de propósito general con 30GB de RAM y 4 CPU. En base a los resultados de la primer prueba, evaluaría utilizar instancias optimizadas para storage o memoria.

### Scaling

La solución propuesta solo escala automáticamente agregando nodos cuando detecta que queda poco espacio de storage disponible en el cluster.

Tendría que hacer pruebas más exhaustivas para poder ver si conviene escalar por un pico de tráfico. En principio no lo creo conveniente, ya que agregar nodos al cluster solo empeoraría la situación de consumo de recursos.

Trataría de hacer un dimensionamiento más concreto al inicio como para que le quede juego para soportar un pico de carga. También se podría preveer si existe un patrón de carga por horarios o fechas y escalar en base a eso en lugar de escalar en base al consumo de recursos.

## Pruebas

Para realizar las pruebas de la solución, se creó un servicio que corre como containers de docker, que manda logs simulados de HAproxy al stream de Kinesis (`kinesis-haproxy-fake-logs`).

Actualmente no estoy pudiendo lograr la carga deseada, porque no logro de Kinesis me procese más de 300K eventos por segundo. Aparentemente para lograr eso hay que usar la librería de producer que solo tiene bindings en java, así que lo próximo va a ser hacer las pruebas usando esa librería.

## Infraestructura

Para los servicios de prueba y auxiliares, se crea un VPC con 2 Availability Zone y en cada AZ, 2 subredes (una privada y una pública). En la red privada se instalan los servicios que tienen acceso a internet a través de un NAT Gateway.

Para el cluster de elasticsearch se utiliza el servicio Amazon Elasticsearch Service. Lo mismo para Kinesis y Firehose. El cluster de elasticsearch se accede desde el VPC con IAM roles y se expone a internet a través de un proxy con autenticación http básica. Este proxy se hace con containers de nginx balanceados con un ALB.

Todo el deploy de la solución está automatizado con los templates de Cloudformation que se encuentran en este repositorio.

La estructura final de la solución se muestra en el siguiente diagrama que se complementa con el anterior:

![Infraestructura](assets/infra.png)


## Repos de los servicios auxiliares y de prueba

https://github.com/reyiyo/elastic-curator-tasks

https://github.com/reyiyo/haproxy-log-generator

https://github.com/reyiyo/env-nginx-proxy

https://github.com/reyiyo/kinesis-haproxy-fake-logs

## Instalación

El script de deploy sube los templates a un bucket de S3, buildea las imágenes de docker correspondientes y las sube a ECR.

`$ bin/deploy.sh`

Luego de esto se puede deplolyar el stack de cloudformation que está en `/master.yaml`.